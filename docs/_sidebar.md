* [📣 1 概述](README.md)

* [☀️ 2 特性和亮点](#)
  
  * [💖 2.1 贴心设计](2_features/1_intimate_design.md)
  * [🌟 2.2 特性演示](#)
    * [⭐ 与 requests 对比](2_features/2_features_demos/1_compare_with_requests.md)
    * [⭐ 与 selenium 对比](2_features/2_features_demos/2_compare_with_selenium.md)
    * [⭐ 模式切换](2_features/2_features_demos/3_switch_mode.md)
    * [⭐ 获取元素属性](2_features/2_features_demos/4_get_element_attributes.md)
    * [⭐ 下载文件](2_features/2_features_demos/5_download_file.md)

* [🧭 3 入门指南](#)
  
  * [🌏 3.1 安装和导入](3_get_start/1_installation_and_import.md)
  * [🌏 3.2 准备工作](3_get_start/2_before_start.md)
  * [🌏 3.3 上手示例](#)
    * [🗺️ 操控浏览器](3_get_start/3_examples/1_control_browser.md)
    * [🗺️ 收发数据包](3_get_start/3_examples/2_data_packets.md)
    * [🗺️ 模式切换](3_get_start/3_examples/3_switch_mode.md)
  * [🌏 3.4 基本概念](3_get_start/4_basic_concept.md)

* [📫 4 SessionPage](#)
  
  * [✉️ 4.0 概述](4_SessionPge/0_introduction.md)
  * [✉️ 4.1 创建页面对象](4_SessionPge/1_create_page_object.md)
  * [✉️ 4.2 访问网页](4_SessionPge/2_visit_web_page.md)
  * [✉️ 4.3 获取网页信息](4_SessionPge/3_get_page_info.md)
  * [✉️ 4.4 查找元素](4_SessionPge/4_find_elements.md)
  * [✉️ 4.5 获取元素信息](4_SessionPge/5_get_element_info.md)
  * [✉️ 4.6 启动配置](4_SessionPge/6_session_options.md)

* [🚀 5 ChromiumPage](#)
  
  * [🛰️ 5.0 概述](5_ChromiumPage/0_introduction.md)
  * [🛰️ 5.1 创建页面对象](5_ChromiumPage/1_create_page_object.md)
  * [🛰️ 5.2 访问网页](5_ChromiumPage/2_visit_web_page.md)
  * [🛰️ 5.3 获取网页信息](5_ChromiumPage/3_get_page_info.md)
  * [🛰️ 5.4 页面交互](5_ChromiumPage/4_page_operation.md)
  * [🛰️ 5.5 查找元素](5_ChromiumPage/5_find_elements.md)
  * [🛰️ 5.6 获取元素信息](5_ChromiumPage/6_get_element_info.md)
  * [🛰️ 5.7 元素交互](5_ChromiumPage/7_element_operation.md)
  * [🛰️ 5.8 标签页操作](5_ChromiumPage/8_tab_operation.md)
  * [🛰️ 5.9 iframe 操作](5_ChromiumPage/9_frame_operation.md)
  * [🛰️ 5.10 动作链](5_ChromiumPage/10_action_chains.md)
  * [🛰️ 5.11 浏览器启动配置](5_ChromiumPage/11_browser_options.md)

* [🌌 6 WebPage](#)
  
  * [🪐 6.0 概述](6_WebPage/0_introduction.md)
  * [🪐 6.1 创建页面对象](6_WebPage/1_create_page_object.md)
  * [🪐 6.2 模式切换](6_WebPage/2_mode_switch.md)
  * [🪐 6.3 独有的功能](6_WebPage/3_webpage_function.md)

* [🧰 7 进阶使用](#)
  
  * [⚙️ 7.1 配置文件的使用](7_advance/1_ini_file.md)
  * [⚙️ 7.2 easy_set 方法](7_advance/2_easy_set.md)
  * [⚙️ 7.3 下载文件](7_advance/3_download.md)
  * [⚙️ 7.4 加速浏览器数据采集](7_advance/4_accelerate_collecting.md)
  * [⚙️ 7.5 打包程序](7_advance/5_packaging.md)
  * [⚙️ 7.6 监听浏览器网络](7_advance/6_monitor_network.md)

* [🛠 8 旧版使用方法](#)
  
  * [🔨 8.0 概述](8_MixPage/0_introduction.md)
  * [🔨 8.1 创建页面对象](8_MixPage/1_create_page_object.md)
  * [🔨 8.2 访问网页](8_MixPage/2_visit_web_page.md)
  * [🔨 8.3 查找页面元素](8_MixPage/3_find_page_element.md)
  * [🔨 8.4 获取元素信息](8_MixPage/4_get_element_info.md)
  * [🔨 8.5 元素操作](8_MixPage/5_element_operation.md)
  * [🔨 8.6 获取网页信息](8_MixPage/6_get_page_info.md)
  * [🔨 8.7 页面操作](8_MixPage/7_page_operation.md)
  * [🔨 8.8 cookies 的使用](8_MixPage/8_cookies.md)
  * [🔨 8.9 Drission 对象](8_MixPage/9_Drission.md)
  * [🔨 8.10 对接 selenium 及 requests 代码](8_MixPage/10_work_with_selenium_and_requests.md)
  * [🔨 8.11 使用其它系统或浏览器](8_MixPage/11_use_other_browser.md)
  * [🔨 8.12 DriverPage 和 SessionPage](8_MixPage/12_DriverPage_and_SessionPage.md)

* [⚡️ 9 示例和技巧](#)
  
  * [🌠 自动登录码云](9_demos/login_gitee.md)
  * [🌠 采集猫眼电影 TOP100 榜](9_demos/maoyan_TOP100.md)
  * [🌠 下载星巴克产品图片](9_demos/starbucks_pics.md)
  * [🌠 下载豆瓣图书封面图片](9_demos/douban_book_pics.md)
  * [🌠 多线程操作多标签页](9_demos/multithreading_with_tabs.md)

* [🔖 10 版本历史](10_history.md)

* [💐 鸣谢](thx.md)
