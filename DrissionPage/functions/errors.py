# -*- coding:utf-8 -*-
class AlertExistsError(Exception):
    pass


class ContextLossError(Exception):
    pass


class ElementLossError(Exception):
    pass
